# 模型迁移训练FAQ

目录：

- [模型迁移训练FAQ](#模型迁移训练faq)
  - [1 MindSpore编码FAQ](#1-mindspore编码faq)
    - [Q1.1: MindSpore是否支持XX算子？与Pytorch算子有何区别？](#q11-mindspore是否支持xx算子与pytorch算子有何区别)
    - [Q1.2: MindSpore有哪些语法上的限制？](#q12-mindspore有哪些语法上的限制)
    - [Q1.3: MindSpore提供的高阶训练接口不能满足训练流程需求，如何编写自定义训练流程？](#q13-mindspore提供的高阶训练接口不能满足训练流程需求如何编写自定义训练流程)
    - [Q1.4: 动态图运行正常的代码切换到静态图后报错？](#q14-动态图运行正常的代码切换到静态图后报错)
    - [Q1.5: 数据下沉模式是什么？有什么作用？](#q15-数据下沉模式是什么有什么作用)
  - [2 模型训练FAQ](#2-模型训练faq)
    - [Q2.1: 动态图模式下运行正常的代码在静态图模式下执行报错？](#q21-动态图模式下运行正常的代码在静态图模式下执行报错)
    - [Q2.2: 如何判断迁移后模型训练流程是否正确？](#q22-如何判断迁移后模型训练流程是否正确)
  - [3 分布式训练FAQ](#3-分布式训练faq)
    - [多卡训练FAQ](#多卡训练faq)
      - [Q3.1: 分布式训练与单卡训练有何区别，有哪些可参考的教程文档？](#q31-分布式训练与单卡训练有何区别有哪些可参考的教程文档)
      - [Q3.2: 单机多卡的分布式训练应该如何配置环境变量？](#q32-单机多卡的分布式训练应该如何配置环境变量)
    - [云上集群训练FAQ](#云上集群训练faq)
  - [4 模型精度调优FAQ](#4-模型精度调优faq)
  - [5 训练性能FAQ](#5-训练性能faq)
    - [Q5.1: 模型训练效率明显低于预期，前反向耗时过长，是什么原因？](#q51-模型训练效率明显低于预期前反向耗时过长是什么原因)
    - [Q5.2: 如何分析模型训练性能？](#q52-如何分析模型训练性能)
    - [Q5.3: 发现数据处理过慢，应该如何解决？](#q53-发现数据处理过慢应该如何解决)

## 1 MindSpore编码FAQ

---

### Q1.1: MindSpore是否支持XX算子？与Pytorch算子有何区别？

**A**：
通常来讲，MindSpore已经支持深度学习中常用的绝大多数算子，可以在[官方API文档](https://www.mindspore.cn/docs/zh-CN/r1.9/index.html)中查找各算子文档及其用法；但部分算子的实现方式和Pytorch有一定的差异，需要注意不要混用，官方文档提供了[API映射表](https://www.mindspore.cn/docs/zh-CN/r1.9/note/api_mapping/pytorch_api_mapping.html)，方便快速查询与Pytorch算子的对应关系及其区别。

此处列举几个常见的区别算子

|Pytorch|MindSpore|不同点|
|-|-|-|
|`torch.transpose`|`mindspore.Tensor.transpose`|MindSpore转置算子的调用方式是重新排列各维度的顺序，以进行多维度间的转置，当仅需转置指定的两个维度时，可以调用[`mindspore.Tensor.swapaxes`](https://www.mindspore.cn/docs/zh-CN/r1.9/api_python/mindspore/Tensor/mindspore.Tensor.swapaxes.html#mindspore.Tensor.swapaxes)接口实现|
|`torch.nn.Dropout`|`mindspore.nn.Dropout`|PyTorch中P参数为丢弃参数的概率；MindSpore中`keep_prob`参数为保留参数的概率，容易混淆|
|`torch.norm`|`mindspore.ops.LpNorm`|PyTorch：p参数可以支持`int`，`float`，`inf`，`-inf`，`fro`，`nuc`等类型或值，以实现不同类型的归一化；MindSpore：目前仅支持整数p范式的归一化|
|`torch.nn.Conv2d`|`mindspore.nn.Conv2d`|PyTorch：默认不对输入进行填充，`bias`为True；MindSpore：默认对输入进行填充，使输出与输入维度一致，如果不需要padding，可以将参数设为’valid’。默认`has_bias`为False|

在需求某些MindSpore尚未实现的算子时，可以通过继承mindspore.nn.Cell的方式进行自定义算子的开发

在此提供官方指导文档用作参考：[与PyTorch典型区别](https://www.mindspore.cn/docs/zh-CN/r1.9/migration_guide/typical_api_comparision.html)

---

### Q1.2: MindSpore有哪些语法上的限制？

**A**：
MindSpore在训练过程中通常使用静态图的模式执行网络计算，以提高训练性能。关于动态图与静态图的概念请参照官方文档：[动静图结合](https://www.mindspore.cn/docs/zh-CN/r1.9/design/dynamic_graph_and_static_graph.html)

在动态图模式下运行时，MindSpore基本没有额外的语法限制，和原生python保持一致；但是在静态图模式下运行时，MindSpore则对模型的`construct`函数存在限制以通过编图流程，具体可以参照官方文档：[静态图语法支持](https://www.mindspore.cn/docs/zh-CN/r1.9/note/static_graph_syntax_support.html)

规避静态图语法限制的方法请参照[Q4](#q14-动态图运行正常的代码切换到静态图后报错)

其余内容可参考：[昇思开发者手册-精度调优常见问题汇总][precision_tuning]

[precision_tuning]: https://ms-gametutorial.readthedocs.io/en/latest/doc/precision-tuning/%E7%B2%BE%E5%BA%A6%E8%B0%83%E4%BC%98%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98%E6%B1%87%E6%80%BB.html

---

### Q1.3: MindSpore提供的高阶训练接口不能满足训练流程需求，如何编写自定义训练流程？

**A**：
MindSpore的`mindspore.Model`提供了`train`，`eval`，`predict`，`fit`等几个高阶接口，方便快速组装训练流程

在预定义的训练流程无法满足需求时，可以使用MindSpore提供的自定义训练流程的接口，在`mindspore.nn.TrainOneStepCell`类实例中编写网络的前向计算，损失函数计算和反向传播等步骤，以实现网络训练流程的自定义

使用MindSpore的自定义训练流程通常包括以下步骤：

1. 用继承`Cell`基类的方式构建网络NetWork
2. 用`WithLossCell`连接NetWork和Loss function
3. 用`TrainOneStepCell`传入2中的cell，进行：
   - 正反向计算
   - 梯度操作
   - 并行reducer
   - 优化器更新权重

具体example可以参考：[官方文档示例](https://www.mindspore.cn/docs/zh-CN/r1.9/api_python/nn/mindspore.nn.TrainOneStepCell.html#mindspore.nn.TrainOneStepCell)，[昇思开发者手册-精度调优常见问题汇总][precision_tuning]和ModelZoo上一些模型训练流程的实现等资源

---

### Q1.4: 动态图运行正常的代码切换到静态图后报错？

**A**:

在MindSpore中，静态图模式又被称为Graph模式，可以通过set_context(mode=GRAPH_MODE)来设置成静态图模式。静态图模式比较适合网络固定且需要高性能的场景。在静态图模式下，基于图优化、计算图整图下沉等技术，编译器可以针对图进行全局的优化，因此**在静态图下能获得较好的性能**，但是执行图是从源码转换而来，因此在静态图下**不是所有的Python语法都能支持**，详细请查看[语法支持](https://www.mindspore.cn/docs/zh-CN/r1.9/note/syntax_list.html)

相较于详细的语法说明，**常见的限制可以归结为以下几点**：

**场景1**：  
限制：构图时（construct函数部分或者用ms_function修饰的函数），不要调用其他Python库，例如numpy、scipy，相关的处理应该前移到__init__阶段。  
措施：使用MindSpore内部提供的API替换其他Python库的功能。常量的处理可以前移到__init__阶段。

**场景2**：  
限制：构图时不要使用自定义类型，而应该使用MindSpore提供的数据类型和Python基础类型，可以使用基于这些类型的tuple/list组合。  
措施：使用基础类型进行组合，可以考虑增加函数参数量。函数入参数没有限制，并且可以使用不定长输入。

**场景3**：  
限制：构图时不要对数据进行多线程或多进程处理。  
措施：避免网络中出现多线程处理。

在进行编码时遵循**以下建议**，即可规避绝大多数静态图下的语法限制：

1. 将所有的Tensor实例化，静态参数初始化，判断条件初始化等内容移至`Cell`的`__init__`过程中，在`construct`函数里只进行模型的前向计算
2. 规避动态shape，在输入数据batch时将`drop_remainder`设置为`True`
3. API使用优先级：
   mindspore.nn > mindspore.ops > mindspore.numpy
4. 尽量剔除掺杂在模型代码里的非Tensor处理代码

---

### Q1.5: 数据下沉模式是什么？有什么作用？

**A**:  
数据下沉是为了减少host侧与device侧之间频繁的通信开销而提出的数据传输方法，能够有效地**提升训练与推理的性能**。

详细概念请参照[官方介绍文档](https://www.mindspore.cn/tutorials/experts/zh-CN/master/optimize/execution_opt.html)，文档位置相对不好检索。

在此列出使用的一些注意点：

- 用户可通过`train`接口的`dataset_sink_mode`控制是否使能数据下沉。
- GPU后端暂不支持图下沉；使用昇腾设备时，开启数据下沉**会同时启用图下沉**。
- 通过`train`接口的`dataset_sink_mode`和`sink_size`参数控制每个epoch的下沉迭代数量，Device侧连续执行`sink_size`个迭代后才返回到Host。
  - 如果`sink_size`为默认值-1，则每一个epoch训练整个数据集，理想状态下下沉数据的速度快于硬件计算的速度，保证处理数据的耗时隐藏于网络计算时间内；
  - 如果`sink_size`>0，此时原始数据集可以被无限次遍历，下沉数据流程仍与`sink_size`=-1相同，不同点是每个epoch仅训练`sink_size`大小的数据量，如果有`LossMonitor`，那么会训练`sink_size`大小的数据量就打印一次loss值，下一个epoch继续从上次遍历的结束位置继续遍历。下沉的总数据量由epoch和`sink_size`两个变量共同控制，即总数据量=epoch*`sink_size`。
- 当使用`LossMonitor`、`TimeMonitor`或其它`Callback`接口时，如果`dataset_sink_mode`设置为False，Host侧和Device侧之间**每个step**交互一次，所以会每个step返回一个结果，如果`dataset_sink_mode`为True，因为数据在Device上通过通道传输，Host侧和Device侧之间**每个epoch**进行一次数据交互，所以每个epoch只返回一次结果。
- 当前CPU不支持数据下沉。
- 当设置为GRAPH模式时，每个batch数据的shape必须相同；当设置为PYNATIVE模式时，要求每个batch的size相同。
- 由于数据下沉对数据集的遍历是连续，当前不支持非连续遍历。
- 如果在使用数据下沉模式时，出现`fault kernel_name=GetNext`、`GetNext... task error`或者`outputs = self.get_next()`等类似的错误，那么有可能是数据处理过程中**某些样本处理太耗时，导致网络计算侧长时间拿不到数据报错**，此时可以将`dataset_sink_mode`设置为False再次验证，或者对数据集使用`create_dict_iterator()`接口单独循环数据集，并参考[数据处理性能优化](https://mindspore.cn/tutorials/experts/zh-CN/master/dataset/optimize.html)调优数据处理，保证数据处理高性能。

---

## 2 模型训练FAQ

---

### Q2.1: 动态图模式下运行正常的代码在静态图模式下执行报错？

**A**:
主要原因为MindSpore静态图执行模式为编译执行，不能支持所有的Python语法；如在静态图模式下执行出现报错，请参考[MindSpore编码相关Q4](#q14-动态图运行正常的代码切换到静态图后报错)中的语法限制说明与相应措施，对代码进行相应修改。

---

### Q2.2: 如何判断迁移后模型训练流程是否正确？

**A**：
从Pytorch迁移至MindSpore的模型，可以采取**前向对比**与**反向对比**等方式，判断迁移后的模型，其模型结构，执行流程与训练流程与对标实现是否一致

**前向对比基本步骤**：

1. 构造相同的模型输入：从数据集中获取或者随机生成相同shape，相同数值的模型输入数据
2. 构造相同的模型权重：编写权重转换脚本，或者采取相同参数初始化，确保需对比的两模型参数值一致
   - Pytroch和MindSpore的模型权重文件都是基于字典列表的形式，所以权重转换其实就是给对应算子的权重key进行重命名的操作；在权重转换的过程中也能够发现缺失/冗余的算子参数，在一定程度上能够帮助定位模型结构差异
3. 实例化模型，加载相同权重，输入相同的数据分别获取前向construct/forward输出结果
4. 使用np.allclose等方法判断前向输出的相似性，可采用双千分之一标准：相对误差小于0.1%，绝对误差小于1e-3
5. 采用二分法等方式逐步对比模型不同部分，定位引起计算差距的代码块/算子

此处提供AI工具部门所开发的[MS-PT模块级精度测试工具](https://gitee.com/wangchao285/ms_pt_compare)作为参考，可以根据具体需求，使用该工具进行比对或者根据上述流程编写测试用例进行前向比对

---

## 3 分布式训练FAQ

本章节主要收集了，在调通单卡训练后，需进行多卡分布式训练时常见的问题

### 多卡训练FAQ

主要收集了在多卡物理机上进行训练时的常见问题

#### Q3.1: 分布式训练与单卡训练有何区别，有哪些可参考的教程文档？

**A**:  
通常，受限于计算卡的计算能力，计算速度，与显存容量等因素，单卡训练的效率往往不能满足当前主流模型的训练需求。

在模型训练过程中，增大计算卡数通常有以下几个目的：

1. **增大模型训练的batch size**；
   batch size大小的选择通常会对模型的收敛效果产生影响，单卡计算能够承载的batch size受限于显存容量，通常被限制在一个比较小的范围里，达不到对模型收敛效果最有利的bs大小；此时通常选择增加训练卡数，采用**数据并行**策略，在一个前反向step内多卡处理更多数据，并聚合多卡计算结果，达到增大batch size的目的
2. **加快训练速度**；
   在数据集大小确定，训练迭代次数确定，计算量基本确定的情况下，单卡计算速度波动不大，可以通过增加卡数的方式，节省计算时间，以达到加快训练的目的
3. **应对显存消耗过大的场景**；
   通常发生在大模型场景下，模型的参数量达到上亿，乃至上百亿千亿规模时，单卡的显存(通常32G左右)无法储存模型训练过程中所需的全部权重值，此时会用到**模型并行**策略，将权重值拆分分别存储至多卡上，使用多卡完成一个batch数据的前反向计算，使得模型能够在显存不足的训练卡上完成训练

相比于单卡训练，多卡训练的额外配置项在于**集群通信**于**并行策略**上；这两部分的内容较为庞杂，无法在单个QA中讲清所有细节，在此列出值得阅读学习的相关文档/教程，希望能够有所帮助：

1. [MindSpore-分布式并行总览](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.10/parallel/introduction.html)
   整体介绍了MindSpore支持的分布式并行训练的几大基础能力，包括*数据并行，模型并行，混合并行*三种并行类型的概念；*数据并行，自动并行，半自动并行，混合并行*几种MindSpore支持的并行模式，以及在MindSpore中应用并行的*通信初始化*方式
2. [分布式并行训练基础样例（Ascend）](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.10/parallel/train_ascend.html)
   以ResNet50为例，详细介绍了在Ascend平台上应用数据并行和自动并行训练能力的全流程
3. [MindSpore分布式并行特性](https://www.mindspore.cn/docs/zh-CN/r1.10/design/distributed_training_design.html)
   较为详细地阐述了MindSpore中*数据并行*和*自动并行*两大特性的工作原理
4. [多维度混合并行](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.10/parallel/multi_dimensional.html)
   进一步针对高阶并行特性作介绍，包括*算子级并行*，*流水线并行*，*优化器并行*，*Host&Device异构*和*重计算*等

#### Q3.2: 单机多卡的分布式训练应该如何配置环境变量？

**A**:  
启动分布式训练时主要有以下几个环境变量：

- `MINDSPORE_HCCL_CONFIG_PATH`或`RANK_TABLE_FILE`: 记录各卡的通信ip，卡号等信息的json配置的文件路径，文件内容样例如下，可使用[hccl_tools.py](tools/hccl_tools.py)脚本生成
  
  ```json
  {
    "version": "1.0",
    "server_count": "1",
    "server_list": [
        {
            "server_id": "10.155.111.140",
            "device": [
                {"device_id": "0","device_ip": "192.1.27.6","rank_id": "0"},
                {"device_id": "1","device_ip": "192.2.27.6","rank_id": "1"},
                {"device_id": "2","device_ip": "192.3.27.6","rank_id": "2"},
                {"device_id": "3","device_ip": "192.4.27.6","rank_id": "3"},
                {"device_id": "4","device_ip": "192.1.27.7","rank_id": "4"},
                {"device_id": "5","device_ip": "192.2.27.7","rank_id": "5"},
                {"device_id": "6","device_ip": "192.3.27.7","rank_id": "6"},
                {"device_id": "7","device_ip": "192.4.27.7","rank_id": "7"}],
             "host_nic_ip": "reserve"
        }
    ],
    "status": "completed"
  }
  ```

- `DEVICE_NUM`: 使用的总卡数
- `RANK_ID`: 使用的卡在集群中的编号，从0开始计数，第几张卡
- `DEVICE_ID`: 设备编号，对应rank_table_file中的`device_id`字段

下面是一个典型的多卡分布式启动脚本，通过for循环，配置不同的`RANK_ID`和`DEVICE_ID`环境变量，并拉起相应的单卡启动脚本，完成多卡拉起训练的流程

```shell
export MINDSPORE_HCCL_CONFIG_PATH=rank_table_file
export DEVICE_NUM=8
BASE_PATH=$(cd "$(dirname $0)"; pwd)
for((i=0; i<$DEVICE_NUM; i++)); do
    rm -rf ${BASE_PATH}/rank${i}
    mkdir ${BASE_PATH}/rank${i}
    cp -r ${BASE_PATH}/train.py ${BASE_PATH}/rank${i}/
    cd ${BASE_PATH}/rank${i}
    export RANK_ID=${i}
    export DEVICE_ID=${i}
    echo "start training for device $i"
    python train.py > training_rank_${i}.log 2>&1 &
done
```

在被拉起的python脚本中，应当调用集合通信接口，建立MindSpore集群通信信息，完成分布式训练的初始化，如下示例代码：

```python
import os
import mindspore as ms
from mindspore.communication import init

if __name__ == "__main__":
    ms.set_context(mode=ms.GRAPH_MODE, device_target="Ascend", device_id=int(os.environ["DEVICE_ID"]))
    init()
    device_num = get_group_size()
    rank = get_rank()
    print("rank_id is {}, device_num is {}".format(rank, device_num))
    ms.reset_auto_parallel_context()
    # 下述的并行配置用户只需要配置其中一种模式
    # 数据并行模式
    ms.set_auto_parallel_context(parallel_mode=ms.ParallelMode.DATA_PARALLEL)
    # 半自动并行模式
    # ms.set_auto_parallel_context(parallel_mode=ms.ParallelMode.SEMI_AUTO_PARALLEL)
    # 自动并行模式
    # ms.set_auto_parallel_context(parallel_mode=ms.ParallelMode.AUTO_PARALLEL)
    # 混合并行模式
    # ms.set_auto_parallel_context(parallel_mode=ms.ParallelMode.HYBRID_PARALLEL)
    ...
```

其中，

- `mode=ms.GRAPH_MODE`：使用分布式训练需要指定运行模式为图模式（PyNative模式当前仅支持数据并行）。
- `device_id`：卡的物理序号，即卡所在机器中的实际序号。
- `init()`：使能HCCL通信，并完成分布式训练初始化操作。

这样我们就完成了多卡分布式训练任务的拉起，相较于单机训练脚本，仅需在训练入口调用接口初始化集群通信，并配置相应context，代码改动量整体较少

### 云上集群训练FAQ

---

## 4 模型精度调优FAQ

---

## 5 训练性能FAQ

---

### Q5.1: 模型训练效率明显低于预期，前反向耗时过长，是什么原因？

**A**：  
如果发现模型在MindSpore上的训练效率 ***显著*** 低于同配置下Pytorch的运行效率，如 *5000ms/step对500ms/step*（仅举例），那最有可能的一个原因是使用了**fp32精度**计算矩阵乘运算

M+A架构下，底层NPU芯片对矩阵乘运算进行了适应优化，使其在**fp16精度**下的运算性能有了明显提升；但对fp32精度下的计算优化适配尚不完善，使得该精度下的矩阵乘计算无法有效利用NPU性能，导致了不同精度下计算效率的显著差异

因此该情况下的**解决方式**则为：在**计算矩阵乘前时将tensor降精度为fp16，计算结束后将精度还原至fp32**

例如：

```python
from mindspore import nn, ops
from mindspore import dtype as mstype


class Net(nn.Cell):
    def __init__(self, in_channels, out_channels):
        # 初始化相应算子
        self.linear = nn.Dense(in_channels, out_channels)
        self.bn = nn.BatchNorm2D(out_channels)
        self.relu = nn.ReLU()
        # 类型转换算子
        self.cast = ops.Cast()
    
    def constrcut(x):
        # 计算Dense层前先降精度
        x = self.cast(x, mstype.float16)
        x = self.linear(x)
        # 矩阵乘计算完毕还原精度
        x = self.cast(x, mstype.float32)
        x = self.bn(x)
        x = self.relu(x)

        return x
```

使用以上代码或等价的[`nn.Cell.to_float()`](https://www.mindspore.cn/docs/zh-CN/r1.9/api_python/nn/mindspore.nn.Cell.html#mindspore.nn.Cell.to_float)接口完成计算过程中的精度转换，应当可以解决训练性能的显著差异问题

常见的包含矩阵乘计算的算子有：`ops.matmul()`, `ops.MatMul`, `nn.Dense`, `nn.Conv2d`等，如果你的网络中包含这些算子并且计算量较大，可尝试使用以上的降精度方法

如果以上回答并不能解决你的训练效率问题，请参照[Q2: 如何分析模型训练性能](#q52-如何分析模型训练性能)定位训练性能问题

---

### Q5.2: 如何分析模型训练性能？

**A**：  
MindSpore提供了性能调试的功能，具体可参照[Profiler接口文档](https://www.mindspore.cn/docs/zh-CN/r1.9/api_python/mindspore/mindspore.Profiler.html)和[MindInsight可视化性能调试教程](https://www.mindspore.cn/mindinsight/docs/zh-CN/r1.9/performance_profiling_ascend.html)两篇文档

前者提供了在训练过程中收集分析训练性能表现的能力，后者则提供了将训练性能表现可视化的便捷功能；如下图即可看到一个训练step中各训练阶段所花费时间占比，各算子执行时间占比与数据处理时间等多维度的性能表现

![img](https://www.mindspore.cn/mindinsight/docs/zh-CN/r1.9/_images/performance_overall.png)

善用以上工具可以获取到训练流程中性能表现的有效信息，并根据信息具体分析，进行有方向性能调优，在有性能需求时应当掌握

TODO：性能分析实践示例文档

---

### Q5.3: 发现数据处理过慢，应该如何解决？
