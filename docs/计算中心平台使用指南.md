# ModelArts 开发操作指南

## 一、前期准备

### 1. 登录进入计算中心平台

* [武汉计算中心登录链接](https://login.ovaijisuan.com/auth/realms/userconsole/protocol/openid-connect/auth?client_id=rightcloud-bss&redirect_uri=https%3A%2F%2Fuconsole.ovaijisuan.com%2F%23%2F&state=e20cb881-fc42-4829-8141-35334ebb36e2&response_mode=fragment&response_type=code&scope=openid&nonce=92343d62-c43b-4f2a-8795-02508051e028)

    > 其他地域的计算中心登录链接可联系华为接口人获取

* 请联系华为项目接口人获取计算中心平台账号（同一个项目只申请一个账号）

* 计算中心控制台界面如下：

  ![image-20211201154542175](https://i.loli.net/2021/12/01/fkj8ahJ57FWtzDU.png)

### 2. Modelarts使用概览

进入Modelarts控制台后，主要关注下图中开发环境、训练管理、专属资源池和全局配置四个板块内容，后面均会有详细的介绍说明！

![image-20211201155144682](https://i.loli.net/2021/12/01/l4XtWPrY6k5eSsU.png)

## 二、ModelArts 之开发调试

**【说明】： 通常我们的数据集、工程代码、训练输出文件等都会存储在OBS中，因此需要了解OBS的使用方法，同时学会配合Notebook和训练作业来使用！！！**

### 1. OBS存储软件使用

* **OBS下载及登录**

  由于obs桶创建后，在计算中界面上直接上传文件存在大小限制，无法上传大文件，因此**推荐使用OBS Browser Plus 软件进行桶的创建和数据上传**

  * 下载链接：

    [OBS Browser Plus 软件Windows64位](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/win64/OBSBrowserPlus-HEC-win64.zip)

    [OBS Browser Plus 软件Windows32位](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/win32/OBSBrowserPlus-HEC-win32.zip)

    [OBS Browser Plus 软件Mac](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/mac/OBSBrowserPlus-HEC-mac64.zip)

  * 登录账号：

    下载成功后，按照提示进行安装，**登录账号请联系华为接口人或者项目组负责人获取！！！**

* **OBS桶内创建相关文件夹**

  由于后续在ModelArts平台上使用OBS存储方式进行模型开发时，需要提前在OBS中**存储数据集、工程代码文件、训练/评估/推理输出结果**等文件，**因此建议提前在自己的obs桶中建立相关文件夹。**

* [OBS使用文档](https://support.huaweicloud.com/obs/)
* [服务器使用obsutils指南](https://support.huaweicloud.com/utiltg-obs/obs_11_0003.html)
  
* **OBS相关语法框架moxing**

  这里需要开发者提前了解华为提供的MoXing语法，学习链接：[MoXing开发指南](https://support.huaweicloud.com/moxing-devg-modelarts/modelarts_11_0001.html)

### 2. NoteBook 实例创建

**Step1**：点击Modelarts控制台中开发环境下的NoteBook，[进入NoteBook创建界面](https://console.ovaijisuan.com/modelarts/?region=cn-central-221#/notebook)

![notebook](https://i.loli.net/2021/12/01/wi2q46QVkaAbFI8.png)

**Step2**: 点击创建后，我们将看到以下界面：填入自定义名称、推荐开启自动停止功能（节省资费）、选择公共镜像、选择公共资源池、根据镜像类型及模型任务选择Ascend\GPU\CPU类型及规格。

**注意**： 这里创建的Notebook通常可以用于实时修改代码和单机调试，但是可能机器环境和实际云上环境不同，所以可作为代码实时修改的工具之一，方便快速在云上调试。

![image-20211201160322877](https://i.loli.net/2021/12/01/HknjfhzPKQlEbXI.png)

**Step3**： 完成NoteBook配置后，按照提示“下一步”并“提交”，完成NoteBook实例的创建。

**Step4**： 点击“打开JupyterLab“后，和Anaconda中 Jupyter NoteBook一样，会打开一个网页版的操作界面。

![jupyterlab](https://i.loli.net/2021/12/01/URWSD7nyepm9vgs.png)

**step5**： 进入Notebook开发界面，可以看到我们项目的代码，这里既可以实时修改，也可以在线调试。

![image-20211201160948257](https://i.loli.net/2021/12/01/mCQJyOtcISzNgD7.png)

## 三、ModelArts 之训练管理

我们准备好obs的数据集和工程代码之后，就可以使用“训练管理”服务来进行“训练作业”的创建，这里我们使用的是专属资源池来进行任务的训练，因此需选择自定义算法和自定义镜像，详细流程如下。

**值得注意的是，多模态项目并未使用obs存储数据集，而是采用SFS Turbo服务来进行数据挂载读取，因此可直接参考[训练作业配置2](#conf)，来创建训练作业。**

### 1. 训练作业创建

* **训练作业配置1（obs数据集）**

![conf1.1](https://i.loli.net/2021/12/01/HdxqeRJF5lwOYX2.png)

![conf1.2](https://i.loli.net/2021/07/23/8LHVrjPKCuWgfUQ.png)

* <b id="conf">训练作业配置2（SFS Turbo 挂载数据集）</b>

  SFS Turbo挂载，由于自动化所很多数据集都非常大（15T），训练时使用obs进行拷贝读取十分耗时，因此使用“弹性文件服务”，可以实现几十T存储盘的挂载，使得训练作业时读取数据和离线物理机读取一致。

![image-20211201164655472](https://i.loli.net/2021/12/01/pt1a3iJqv9CYAXl.png)

![image-20211201165007000](https://i.loli.net/2021/12/01/Ku67If9hNgLd4CD.png)

* **训练任务运行**

    ![training](https://i.loli.net/2021/07/23/lQhZbwNrSuk3CsH.png)

这里可通过**日志**实时查看模型训练的状态输出，方便开发者debug训练代码。另外还可以查看资源占用情况，分析工程代码对服务器资源的利用率。

### 2. 训练作业配置说明

**【注意】：** 需要开发者了解以下几点：**1~3点是OBS读取数据集方式，4点是SFS Turbo数据集读取方式**

* **1、由于训练服务开启时，ModelArts会自动申请一块新的内存环境，将指定的obs代码目录下所有文件copy到/home/work/user-job-dir/目录下，即会在内部自动执行以下命令：**

  ```python
  import moxing as mox
  src_url = "s3://your_bucket_name/src/mindvision" # 和"obs://..."等价
  cur_job_path = "/home/work/user-job-dir/mindvision"
  mox.file.copy_parallel(src_url, cur_job_path)
  ```

* **2、启动训练脚本train.py参数配置需要和相应的config文件中配置路径一致，即在上面configs文件中针对faster-rcnn的配置文件路径均要做相应更改，比如：**

  ```text
  /detection/configs/base/datasets/faster_rcnn_datasets.yaml文件中:
  
  ann_file："/home/ma-user/work/dataset/annotations/instances_train2017.json"  更改为：
  
  ann_file："/home/work/user-job-dir/dataset/annotations/instances_train2017.json"
  
  其他路径同理，全部更改到实际训练时的工程路径即可!!!
  ```

* **3、另外，即使在ModelArts上创建训练任务时指定了obs数据集的路径，但MindSpore系列的源码目前并不支持直接从obs桶中读取数据集，而是采用MoXing框架将obs桶内数据集copy到训练的工程目录下，即会执行以下代码：**

  ```python
  # if the code runs in ModelArts, copy train dataset to ModelArts Training Workspace
  if args.is_modelarts:# 当运行在ModelArts训练管理作业上时
      if not os.path.exists(args.train_data): # 在创建任务时指定的数据集路径，需要和config文件中配置一致
          os.makedirs(args.train_data)
      mox.file.copy_parallel(args.data_url, args.train_data) # 将obs桶内的数据集整个copy到ModelArts训练作业的工作目录下
  ```

* **4、SFS Turbo挂载方式训练作业在配置时，使用自定义的算法配置来进行训练作业，其中运行命令如下：**

  ```shell
  /bin/bash /home/work/run_train.sh 's3://muti-modal/code/uniter-three/'(代码目录) 'uniter-three/pretrain_three_ms.py' （执行命令） '/tmp/log/train.log'（运行log） --'data_url'='s3://zdhs/data/small_data/'（训练数据集，sfs turbo模式下无用，但需要写上） --'train_url'='s3://muti-modal/output/'（训练输出目录，sfs turbo模式下无用，但需要写上）
  ```

### 3. ModelArts 之本地 IDE 开发(Pycharm)

#### [1] 下载安装Pycharm 专业版

请点击 [Pycharm官方下载地址](https://www.jetbrains.com/pycharm/download/other.html) 下载并安装到本地（**Pycharm版本>2019.2 专业版**）

#### [2] NoteBook 实例创建

参考ModelArts开发案例中的 [2. NoteBook 实例创建](#2-notebook-实例创建)， 在最后一栏中**打开SSH远程开发功能**并创建密钥对和远程访问白名单。

**注意要保存好创建生成的密钥对（第一次生成创建时有下载界面，之后不再提供下载），SSH远程登录时需要使用**.

![EVS](https://i.loli.net/2021/07/26/ILmSFuW6Ejazpew.png)

#### [3] Pycharm SSH远程连接

* **Step1：** 创建NoteBook成功后，点击创建的NoteBook实例，查看远程访问信息，主要关注图中标红的信息，包括远程访问的用户名、Host地址、端口号、访问机器的IP地址。

  ![conf](https://i.loli.net/2021/07/26/JZqYOWQlUeLaANv.png)

* **Step2：** 打开Pycharm的Setting选项，选择Tools下的SSH Configuration，按照上述信息，填写SSH远程访问选项。

  ![settings](https://i.loli.net/2021/07/26/ibI3a2D9ehRCBYO.png)

* **Step3：** 利用Pycharm打开远程访问的挂载目录，方便开发者实时进行开发上传。

  * 打开Tools下的Deployment的Configuration选项

  ![deployment](https://i.loli.net/2021/07/26/WUXvyzEpCiFh6MN.png)

  * 配置远程NoteBook访问目录

    ![sftp](https://i.loli.net/2021/07/26/gpi34v1nkWTKJs6.png)

  * 打开远程NoteBook工程目录

    ![remote](https://i.loli.net/2021/07/26/HDFoI25ZykhExfA.png)

至此，相信开发者能够很顺利的完成SSH远程连接NoteBook，下面将讲述如何利用Pycharm来进行工程开发。

#### [4] Pycharm SSH远程调试及开发

在ModelArts的官方使用手册中，已经详细讲述了如何设置Pycharm来利用NoteBook进行远程开发，这里给出使用文档链接。

请主要阅读：[本地IDE开发之Pycharm配置](https://support.huaweicloud.com/engineers-modelarts/modelarts_30_0015.html)

从中我们可以了解到以下几点：

* **利用Pycharm获取NoteBook开发环境预置的虚拟环境路径**
* **利用Pycharm配置ModelArts上的Python Interpreter**
* **ModelArts中NoteBook环境依赖库安装**
* **利用Pycharm在开发环境中调试代码**
