# ModelArts Ascend自定义镜像制作教程

## ModelArts基础镜像

参考武汉计算中心的AIHUB拉取一个基础镜像：<http://ai.hqcases.com/mirrors.html>

## 软件包准备

按照以下目录结构进行准备：

```shell
# 目录结构
ModelArts
    install_packages
        requirements.txt # 需要额外安装的第三方包列表
        mindformers # mindformers包，其中包含即时编译的wheel包，当前采用即时编译wheel包方式离线安装
        Ascend-cann-toolkit_6.3.RC1_linux-aarch64.run  # 需要安装的CANN包，注意是.run结尾
        Ascend-hdk-910-npu-driver_23.0.rc1_linux-aarch64.run  # 需要安装的驱动包，注意是.run结尾
    Dockerfile # Dockerfile文件，用于自动创建自定义镜像
    pip.conf #用于更新镜像内默认的pip源，使用华为源
```

`样例位置：192.168.89.35 switch-root  /home/switch-root/dockerfile/ModelArts`

mindspore配套CANN包、驱动包的下载参考官网，下载社区版即可：<https://www.mindspore.cn/install>

## 文件说明

`install_packages`目录中存放下载好的CANN包和驱动包，由于变更MindSpore版本时，镜像中只需要更新CANN包和对应驱动包即可；

`mindformers`目录是直接从mindformers官网`git clone https://gitee.com/mindspore/mindformers.git`下载，`cd mindformers`后执行`python setup.py bdist_wheel`即时编译一个mindformers的wheel包，它将存放在`mindformers/dist`目录下

`requirements.txt`文本如下显示格式，代表了需要在线安装的第三方库名称和版本，可根据自定义需求进行填写；

```text
numpy
sentencepiece
ftfy
regex
tqdm>=3.3.0
pyyaml
jieba
sympy
```

`pip.conf`默认是华为源，用于替换镜像中已存在的源，保证在ModelArts上可以正常pip安装

```shell
[global]
index-url = https://repo.huaweicloud.com/repository/pypi/simple
trusted-host = repo.huaweicloud.com
timeout = 120
```

`Dockerfile`是构建自定义镜像的核心文件，`docker`将使用该文件进行新的镜像构建

下面对`Dockerfile`文件进行解析

```dockerfile
# FROM后跟当前已下载的镜像全称或者images ID，可以通过docker images命令查询
FROM swr.cn-central-221.ovaijisuan.com/wuhanpublic/mindspore_1_10_1:mindspore_1.10.1-cann_6.0.1-python_3.9-euler_2.8

# 使用华为开源镜像站提供的 pypi 配置
RUN mkdir -p /home/ma-user/.pip/
COPY --chown=ma-user:ma-group pip.conf /home/ma-user/.pip/pip.conf

# 安装CANN包和驱动需要root权限，使用USER将权限切换至root用户
USER root
# ModelArts基础镜像基本是基于欧拉系统，因此内置yum命令，使用以下命令进行一些旧包的更新
RUN yum update -y

# 将当期目录下的install_packages文件夹 添加到镜像的 /home/ma-user/install_packages下，方便内部使用安装
ADD ./install_packages /home/ma-user/install_packages

# 更改文件目录下权限，避免由于权限问题安装失败
RUN chmod +x /home/ma-user/install_packages -R
# 安装Ascend驱动
RUN /home/ma-user/install_packages/Ascend-hdk-910-npu-driver*.run --docker --quiet
ENV LD_LIBRARY_PATH=/usr/local/Ascend/driver/lib64/common:/usr/local/Ascend/driver/lib64/driver:${LD_LIBRARY_PATH}
# 安装Ascend CANN包
RUN /home/ma-user/install_packages/Ascend-cann-toolkit*.run --upgrade

# 修改CANN包及MindFormers包中的离线wheel包权限，后续安装需要使用ma-user权限进行安装
RUN chmod 777 /usr/local/Ascend/ascend-toolkit/latest/lib64/te-*-py3-none-any.whl
RUN chmod 777 /usr/local/Ascend/ascend-toolkit/latest/lib64/hccl-*-py3-none-any.whl
RUN chmod 777 /usr/local/Ascend/ascend-toolkit/latest/toolkit/tools/hccl_parser*.whl
RUN chmod 777 /home/ma-user/install_packages/mindformers/dist/mindformers-*.whl

# 接下来开始安装第三方包，由于ModelArts镜像都是以ma-user的权重启动使用，因此我们将用户切换至ma-user，安装各类第三方包
USER ma-user
# 设置容器镜像预置环境变量
# 将 python 解释器路径加入到 PATH 环境变量中
# 请务必设置 PYTHONUNBUFFERED=1, 以免日志丢失
ENV PATH=${ANACONDA_DIR}/envs/${ENV_NAME}/bin:$PATH \
    PYTHONUNBUFFERED=1

# 更新pip本身
RUN /home/ma-user/anaconda/bin/pip install --upgrade pip
# 安装te topi hccl_parser通信包 属于mindspore功能的一些依赖包
RUN /home/ma-user/anaconda/bin/pip install --user /usr/local/Ascend/ascend-toolkit/latest/lib64/te-*-py3-none-any.whl --force-reinstall
RUN /home/ma-user/anaconda/bin/pip install --user /usr/local/Ascend/ascend-toolkit/latest/lib64/hccl-*-py3-none-any.whl --force-reinstall
RUN /home/ma-user/anaconda/bin/pip install --user /usr/local/Ascend/ascend-toolkit/latest/toolkit/tools/hccl_parser*.whl --force-reinstall
# 卸载旧的mindspore包，这里mindspore-ascend是2.0以前的报名， 2.0(含)以上统一叫 mindspore
RUN /home/ma-user/anaconda/bin/pip uninstall mindspore-ascend -y  # 基础镜像中mindspore版本在2.0(含)以上，这里需要修改为mindspore
# 安装对应版本的mindspore包，链接从mindspore官网复制过来，这里是2.0.0rc1
RUN /home/ma-user/anaconda/bin/pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/2.0.0rc1/MindSpore/unified/aarch64/mindspore-2.0.0rc1-cp39-cp39-linux_aarch64.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple

# 安装/home/ma-user/install_packages/requirements.txt自定义的第三方依赖包
RUN /home/ma-user/anaconda/bin/pip install -r /home/ma-user/install_packages/requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
# 安装mindformers包
RUN /home/ma-user/anaconda/bin/pip install --user /home/ma-user/install_packages/mindformers/dist/mindformers-0.3.0-py3-none-any.whl -i https://pypi.tuna.tsinghua.edu.cn/simple

# 以下为镜像设置新的CANN包环境变量
# control log level. 0-DEBUG, 1-INFO, 2-WARNING, 3-ERROR, 4-CRITICAL, default level is WARNING.
ENV GLOG_v=2

# lib libraries that the run package depends on
ENV LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/lib64:/usr/local/Ascend/driver/lib64:/usr/local/Ascend/ascend-toolkit/latest/opp/built-in/op_impl/ai_core/tbe/op_tiling:${LD_LIBRARY_PATH}

# Environment variables that must be configured
## TBE operator implementation tool path
ENV TBE_IMPL_PATH=/usr/local/Ascend/ascend-toolkit/latest/opp/built-in/op_impl/ai_core/tbe
## OPP path
ENV ASCEND_OPP_PATH=/usr/local/Ascend/ascend-toolkit/latest/opp
## AICPU path
ENV ASCEND_AICPU_PATH=${ASCEND_OPP_PATH}/..
## TBE operator compilation tool path
ENV PATH=/usr/local/Ascend/ascend-toolkit/latest/compiler/ccec_compiler/bin/:${PATH}
## Python library that TBE implementation depends on
ENV PYTHONPATH=${TBE_IMPL_PATH}:${PYTHONPATH}

# 校验mindspore版本安装正确性
RUN python -c "import mindspore;mindspore.run_check()"

# 切换回root用户，删除临时缓存的文件
USER root
RUN rm -rf /home/ma-user/install_packages
RUN echo 'source /usr/local/Ascend/ascend-toolkit/set_env.sh' >> ~/.bashrc

# 切换回ma-user用户
USER ma-user
# 设定默认的工作目录，和ModelArts默认工作目录保持一致
WORKDIR /home/ma-user
```

## 镜像创建及推送

准备好相应的文件后，使用docker执行以下命令进行新的自定义镜像构建，注意需要和`Dockerfile`文件同级目录下执行：

```shell
# . 代表当前目录下的Dockerfile文件
# 后面接：镜像名称:版本命名
# 将会根据Dockefile创建新版本ModelArts镜像
docker build . swr.cn-central-221.ovaijisuan.com/mindformers/modelarts_mindspore_2_0_0_rc1:mindformers_dev_py39
```

**注意：**自定义的镜像名称需要和自己所在ModelArts平台的相应域名和组织名程对应，这样才能推送到相应计算中心进行使用，具体可参考文档说明：[大模型云上训练教程-3 镜像配置](%E5%A4%A7%E6%A8%A1%E5%9E%8B%E4%BA%91%E4%B8%8A%E8%AE%AD%E7%BB%83%E6%95%99%E7%A8%8B.md#3-镜像配置)

```shell
# 创建完成后，使用以下命令进行推送
docker push swr.cn-central-221.ovaijisuan.com/mindformers/modelarts_mindspore_2_0_0_rc1:mindformers_dev_py39
```
