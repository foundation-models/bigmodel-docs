# 大模型业务材料

## 介绍

本仓库包含了大模型业务中的常用材料，以帮助您快速上手大模型业务，并提供业务流程中的各方面帮助

## 内容

1. [模型训练迁移FAQ](FAQ/README.md)
   - 汇总了MindSpore+Ascend架构下模型的迁移，训练业务中常见的问题与解答
   - 如果没有覆盖到你的问题，欢迎在本仓库提出issue，我们会尽快答复

2. [MindInsight工具使用]

3. [MindSpore使用系列培训](https://openi.org.cn/html/video/jixun/)

4. [大规模分布式训练]

5. [MindFormers套件使用和开发]
   - 官方文档: <https://mindformerdocstest.readthedocs.io/en/latest>

## 贡献

您可以通过以下方式对本仓库进行贡献：

1. 提交issue
   如果仓库还没有包含您所需的材料，请向本仓库提交issue，说明您的需求，我们将会进行解答，并视情况将您的需求材料加入仓库
2. 提交pull request
   如果您已经积累了一些有用的业务材料，您可以通过fork本仓库，进行文档编写，并以提交PR的方式参与贡献

## 求助方式

如果本业务材料库中的内容无法解决你遇到的问题，需要更多帮助，请尝试以下两种求助方式

### 方式一

- 登录昇腾社区论坛：<https://bbs.huaweicloud.com/forum/forum-1076-1.html>

- 搜索查询是否有类似问题和解决方案（尝试自行解决）

- 根据如下规范发问题求助帖：

  - 求助帖样例：<https://bbs.huaweicloud.com/forum/thread-170993-1-1.html>
  - 发帖图示：
   ![image-20211130113748852](https://i.loli.net/2021/12/01/a4WhdzUcjQsxuDB.png)
  - 发完贴后将帖子链接直接发送到华为项目接口人，走紧急处理流程

### 方式二

[MindSpore gitee官网](https://gitee.com/mindspore/mindspore) 提issue（**判定为mindspore机制bug或者未满足的需求**，须通过issue推动解决）
  
- Step1: 进入MindSpore开源仓新建一个Issue [MindSpore gitee官网新建Issue](https://gitee.com/mindspore/mindspore/issues)
  
   ![image-20220330154818783](https://s2.loli.net/2022/03/30/WE8tbxaBAgFH62Z.png)
  
- Step2：按照以下规范填写Issue内容，请点击**“创建”**进入下一步， 请勿直接点击**“创建并继续”**！！！
  
  ![image-20220330160332720](https://s2.loli.net/2022/03/30/tZlYFGk6fRS4PXM.png)
  
  ![image-20220330160747452](https://s2.loli.net/2022/03/30/jdiGAOw2Ssb6yEl.png)
  
**Tips**： mindspore上面issue填写过于复杂，可直接复制以下信息再填入自己的关键内容
  
```text
### 问题描述

*****请描述自己的问题

### 环境信息
- **Hardware Environment(`Ascend`/`GPU`/`CPU`)  / 硬件环境**: 
> Please delete the backend not involved / 请删除不涉及的后端:
> /device ascend

- **软件环境**:
-- MindSpore version (e.g.,r1.6 commit_id=xxxx) : r1.6
-- Python version (e.g., Python 3.7.5) : Python 3.7.5
-- OS platform and distribution (e.g., Linux Ubuntu 16.04): openEuler 21.04
-- GCC/Compiler version (if compiled from source): gcc version 7.3.0 (GCC)

- **执行模式(`PyNative`/`Graph`)**: 
> 请删除不涉及的模式:
> /mode pynative
> /mode graph

### Describe the expected behavior / 预期结果

### Related log / screenshot / 日志 / 截图

```

- Step3：创建好Issue之后需按照以下规范指定**负责人和标签**

 ![image-20220330161039358](https://s2.loli.net/2022/03/30/fXckCFi2JVM6DYw.png)
  
- Step4：将Issue链接发给华为昇腾大模型研发接口人，推动解决！
